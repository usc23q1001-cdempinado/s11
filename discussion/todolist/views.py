from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from .forms import TodoForm, EventForm, UserForm
import datetime
from .models import ToDoItem, EventItem, User

def index(request):
	todoitem_list = ToDoItem.objects.all()
	eventitem_list = EventItem.objects.filter(user_id=request.user.id)
	
	context = {
		'todoitem_list': todoitem_list,
		'eventitem_list': eventitem_list,
		'user': request.user
		}

	return render(request, "todolist/index.html", context)

def todoitem(request, todoitem_id):
	
	todoitem = model_to_dict(ToDoItem.objects.get(pk=todoitem_id))

	return render(request, "todolist/todoitem.html", todoitem)

def eventitem(request, eventitem_id):
	eventitem = model_to_dict(EventItem.objects.get(pk=eventitem_id))

	return render(request, "todolist/eventitem.html", eventitem)

def taskForm(request):
	context = {}
	context['form'] = TodoForm()
	return render(request, "todolist/taskForm.html", context)

def eventForm(request):

	context={'form':  EventForm()}
	return render(request, "todolist/eventForm.html", context)

def eventNew(request):
	if request.method == "POST":
			form = EventForm(request.POST or None)
			if form.is_valid():
				event = EventItem()
				event.event_name = form.cleaned_data['event_name']
				event.description = form.cleaned_data['description']
				event.status = 'pending'
				event.event_date = datetime.datetime.now() + datetime.timedelta(days=5)
				event.user_id = request.user
				event.save()
				return redirect("index")
			
def taskNew(request):
	if request.method == "POST":
			form = TodoForm(request.POST or None)
			if form.is_valid():
				task = ToDoItem()
				task.task_name = form.cleaned_data['task_name']
				task.description = form.cleaned_data['description']
				task.status = 'pending'
				task.date_created = datetime.datetime.now()
				task.save()
				return redirect("index")
				
def register(request):
	users = User.objects.all()
	is_user_registered = False
	context = {
	"is_user_registered": is_user_registered
	}

	for indiv_user in users:
		if indiv_user.username == "johndoe":
			is_user_registered = True
			break

	if is_user_registered == False:
		user = User()
		user.username = "johndoe"
		user.first_name = "John"
		user.last_name = "Doe"
		user.email = "john@mail.com"
	
		user.set_password("john1234")
		user.is_staff = False
		user.is_active = True
		user.save()
		context = {
			"first_name": user.first_name,
			"last_name": user.last_name
		}

	return render(request, "todolist/register.html", context)


def change_password(request):
	is_user_authenticated = False

	user = authenticate(username="johndoe", password="johndoe1")
	print(user)

	if user is not None:
		authenticated_user = User.objects.get(username='johndoe')
		authenticated_user.set_password("johndoe1")
		authenticated_user.save()
		is_user_authenticated = True

	context = {
		"is_user_authenticated": is_user_authenticated
	}

	return render(request, "todolist/change_password.html", context)

def login_view(request):
	username = "johndoe"
	password = "john1234"

	user = authenticate(username=username, password=password)
	print(user)

	if user is not None:
		login(request, user)
		return redirect("index")

	else:
		context = {
			"is_user_authenticated": False
		}

		return render(request, "todolist/login.html",context)
def login_new(request):
	username = "newUser"
	password = "56&E0x@0"
	user = authenticate(username=username, password=password)
	print(user)
	if user is not None:
		login(request, user)
		return redirect("index")
	else:
		context = {
			"is_user_authenticated": False
		}
		return render(request, "todolist/login.html",context)

def logout_view(request):
	logout(request)
	return redirect("index")

def editForm(request):
	user = User.objects.get(id=request.user.id)
	context = {
		"form": UserForm(initial=model_to_dict(user)),
	}
	return render(request, "todolist/userEditForm.html", context)

def editUser(request):
	if request.method == "POST":
		form = UserForm(request.POST or None)
		if form.is_valid():
			user = User.objects.get(id=request.user.id)
			user.first_name = form.cleaned_data['first_name']
			user.last_name = form.cleaned_data['last_name']
			if form.cleaned_data['password'] == form.cleaned_data['c_password']:
				user.set_password(form.cleaned_data['password'])
				user.save()
			else:
				return HttpResponse("Passwords do not match")
			return redirect("index")