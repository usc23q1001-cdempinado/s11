from django.urls import path
from . import views

urlpatterns = [
	path('', views.index, name='index'),
	
	path('tasks/<int:todoitem_id>/', views.todoitem, name='viewtodoitem'),
  path('events/<int:eventitem_id>/', views.eventitem, name='vieweventitem'),

	path('register', views.register, name='register'),
	path('change_password', views.change_password, name="change_password"),
	path('login', views.login_view, name="login"),
	path('logout', views.logout_view, name="logout"),
  
	path('login_new', views.login_new, name="login_new"),

	path('tasks', views.taskForm, name="tasks"),
  path('events', views.eventForm, name="events"),
  path('events/new', views.eventNew, name="new_event"),
  path('tasks/new', views.taskNew, name="new_task"),
  
	path('edit', views.editForm, name="edit"),
  path('editUser',views.editUser, name="editUser"),
]