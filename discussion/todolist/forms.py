from django import forms

class TodoForm(forms.Form):
    task_name = forms.CharField(label='Task Name', max_length=100)
    description= forms.CharField(label='Description', max_length=500)

class EventForm(forms.Form):
    event_name = forms.CharField(label='Event Name', max_length=100)
    description= forms.CharField(label='Description', max_length=500)

class UserForm(forms.Form):
    first_name = forms.CharField(label='First Name', max_length=100)
    last_name = forms.CharField(label='Last Name', max_length=100)
    password = forms.CharField(label='Password', max_length=100, widget=forms.PasswordInput)
    c_password = forms.CharField(label='Confirm Password', max_length=100, widget=forms.PasswordInput)
